/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "tree.h"

tree * TREE_NewTree () {
    //    printf("TREE_New\n");
    tree * t = (tree *)malloc(sizeof(tree));
    strcpy(t->id, "* ");
    t->h = 0;
    t->b = NULL;
    t->up = NULL;
    t->left = TREE_NewNode(t, 0);
    t->right = TREE_NewNode(t, 1);
    return t;
}

tree * TREE_NewNode (tree * nodeabove, int side) {
    //    printf("TREE_New\n");
    tree * t = (tree *)malloc(sizeof(tree));
    t->up = nodeabove;
    t->left = NULL;
    t->right = NULL;
    t->h = nodeabove->h + 1;
    t->b = BUCK_New();
    strcpy(t->id, nodeabove->id);
    if (side == 0){
        strcat(t->id, "1");
        if (nodeabove->b){
            t->b = BUCK_Split(nodeabove->b, 1, t->h);
        }
    }
    else {
        strcat(t->id, "0");
        if (nodeabove->b){
            t->b = BUCK_Split(nodeabove->b, 0, t->h);
        }
    }
    return t;
}

int TREE_Contract (tree * t) {
    //    printf("TREE_Contract\n");
    return 0;
}

int TREE_Expand (tree * t) {
//    printf("TREE_Expand\n");
    if (t->b){
        t->left = TREE_NewNode(t, 0);
        t->right = TREE_NewNode(t, 1);
        t->b = NULL;
    }
    else{
        TREE_Expand(t->left);
        TREE_Expand(t->right);
    }
    return 0;
}

int TREE_GetTreeHeight (tree * t) {
    if (t){
        tree * t_aux = t;
        while (t_aux->up) {
            t_aux = t->up;
        }
        int cont = 0;
        while (t_aux->left) {
            t_aux = t_aux->left;
            cont++;
        }
        return cont;
    }
    return 0;
}

int TREE_GetNodeHeight (tree * t) {
    if (t){
        tree * t_aux = t;
        int cont = 0;
        while (t_aux->left) {
            t_aux = t_aux->left;
            cont++;
        }
        return cont;
    }
    return 0;
}



void TREE_Print (tree * t) {
//    printf("TREE_Print\n");
    if (t->b) {
        printf("\nARVORE (%s)", t->id);
        if (!BUCK_Empty(t->b)) {
            BUCK_Print(t->b);
        }
    }
//    BUCK_Print(t->b);
    if(t->left){
        TREE_Print(t->left);
        TREE_Print(t->right);
    }
}