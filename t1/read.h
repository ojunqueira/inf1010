/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_read_h
#define EDA_T1_read_h

#include <stdio.h>

/*
READ_JUMPHEADER
 Recebe um arquivo e pula o cabeçalho do mesmo;
 input  = {
    FILE * f: ponteiro para um arquivo do tipo FILE;
 }
*/
void READ_JumpHeader(FILE * f);

/*
READ_NEWSTAR
 Recebe um arquivo e retorna a leitura de uma nova estrela;
 input  = {
    FILE * f:         ponteiro para um arquivo do tipo FILE;
    char * name:      ponteiro para uma string aonde deve ser armazenado o nome da estrela;
    char * class:     ponteiro para uma string aonde deve ser armazanada a classe da estrela;
    float * velocity: ponteiro para um inteiro aonde será armazenada a velocidade da estrela;
 }
 output = {
    int:
        - 1, caso a leitura de uma nova estrela tenha sido feita com sucesso;
        - 0, caso não ocorra a leitura de uma estrela;
 }
*/
int READ_NewStar (FILE * f, char * name, char * class, float * velocity);

/*
READ_BIT
 Retorna o valor de um determinado bit em um número;
 input  = {
    unsigned int number: inteiro, sem sinal, do qual deverá ser lido um bit;
    int index:           inteiro indicando a posição do bit a ser lido (da direita para a esquerda);
 }
 output = {
    int:
        - 0, caso o bit lido seja 0;
        - 1, caso o bit lido seja 1;
 }
*/
int READ_Bit(unsigned int number, int index);

#endif