/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_main_h
#define EDA_T1_main_h

#include <stdio.h>
#include "bucket.h"
#include "hash.h"
#include "read.h"
#include "search.h"
#include "star.h"
#include "tree.h"

#endif