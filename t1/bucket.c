/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "bucket.h"

bucket * BUCK_New () {
    //    printf("BUCK_New\n");
    bucket * b = (bucket *)malloc(sizeof(bucket));
    b->s1 = NULL;
    b->s2 = NULL;
    b->s3 = NULL;
    b->next = NULL;
    return b;
}

bucket * BUCK_Split(bucket * b, int digit, int nbits){
    //    BUCK_Print(b);
    bucket * new = BUCK_New();
    if (b){
        if (b->s1) {
            if (READ_Bit(b->s1->hash, nbits) == digit) {
                BUCK_StarInsert(new, b->s1);
            }
            if(b->s2){
                if (READ_Bit(b->s2->hash, nbits) == digit) {
                    BUCK_StarInsert(new, b->s2);
                }
                if(b->s3){
                    if (READ_Bit(b->s3->hash, nbits) == digit) {
                        BUCK_StarInsert(new, b->s3);
                    }
                }
            }
        }
    }
    return new;
}

void BUCK_Clear (bucket * b) {
    //    printf("BUCK_Clear\n");
    free(b);
    b = NULL;
}

int BUCK_Empty (bucket * b) {
    //    printf("BUCK_Empty\n");
    if (b) {
        if (!b->s1){
            return 1;
        }
    }
    return 0;
}

int BUCK_Full (bucket * b){
    //    printf("BUCK_FULL\n");
    if (b){
        if (b->s1 && b->s2 && b->s3){
            return 1;
        }
        return 0;
    }
    return 0;
}

int BUCK_StarInsert (bucket * b, star * s) {
//    printf("BUCK_StarInsert\n");
    if (b) {
        if (b->s1) {
            if (b->s2) {
                b->s3 = s;
            }
            else{
                b->s2 = s;
            }
        }
        else{
            b->s1 = s;
        }
        return 0;
    }
    return 1;
}

void BUCK_StarRearrange (bucket * b) {
    if (b) {
        if (!b->s1) {
            if (b->s2 && b->s3) {
                b->s1 = b->s3;
                b->s3 = NULL;
            }
            else if (b->s2) {
                b->s1 = b->s2;
                b->s2 = NULL;
            }
            else if (b->s3) {
                b->s1 = b->s3;
                b->s3 = NULL;
            }
        }
        else if (!b->s2) {
            if (b->s3) {
                b->s2 = b->s3;
                b->s3 = NULL;
            }
        }
    }
}

int BUCK_StarRemove (bucket * b, char * name) {
    if (b) {
        if (b->s1) {
            if (strcmp(b->s1->name, name) == 0) {
                b->s1 = NULL;
                BUCK_StarRearrange(b);
                return 0;
            }
        }
        if (b->s2) {
            if (strcmp(b->s2->name, name) == 0) {
                b->s2 = NULL;
                BUCK_StarRearrange(b);
                return 0;
            }
        }
        if (b->s3) {
            if (strcmp(b->s3->name, name) == 0) {
                b->s3 = NULL;
                BUCK_StarRearrange(b);
                return 0;
            }
        }
    }
    return 1;
}

void BUCK_Print (bucket * b){
    //    printf("BUCK_Print\n");
    printf("\n  bucket = <");
    if (b){
        if (b->s1) {
            STAR_Print(b->s1);
        }
        if (b->s2) {
            STAR_Print(b->s2);
        }
        if (b->s3) {
            STAR_Print(b->s3);
        }
        printf("\n  >");
        if (b->next) {
            BUCK_Print(b->next);
        }
    }
}