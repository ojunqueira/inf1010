/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_tree_h
#define EDA_T1_tree_h

#include <stdio.h>
#include <stdlib.h>
#include "bucket.h"

struct Tree {
    char id[35];            /* id = "* " concatenado ao caminho percorrido até o nó ("* " + n X ("1" || "0")) */
    int h;                  /* altura da árvore em relação à raiz */
    bucket * b;             /* bucket relacionado a folha */
    struct Tree * left;     /* nó esquerda (para valor de bit 0) */
    struct Tree * right;    /* nó direita (para valor de bit 1) */
    struct Tree * up;       /* nó acima */
};
typedef struct Tree tree;

/*
TREE_NEWTREE
 Cria nova árvore com duas folhas alocadas dinamicamente;
 input  = {}
 output = {
    tree *:
        - ponteiro para raiz da nova árvore;
        - nil: ERRO ao criar nova árvore;
 }
*/
tree * TREE_NewTree ();

/*
TREE_NEWNODE
 Cria nova árvore alocada dinamicamente;
 nil - erro
 tree - ok
*/
tree * TREE_NewNode (tree * nodeabove, int side);

/*
TREE_EXPAND
 Adiciona uma nova altura a árvore, criando dois filhos para cada folha;
 1 - erro
 0 - ok
*/
int TREE_Expand (tree * t);

/*
TREE_CONTRACT
 Diminui a altura da árvore;
 1 - erro
 0 - ok
*/
int TREE_Contract (tree * t);

/*
TREE_GETTREEHEIGHT
 Retorna a altura da árvore (independente se foi um nó, folha ou raiz fornecido);
*/
int TREE_GetTreeHeight (tree * t);

/*
TREE_GETNODEHEIGHT
 Retorna a altura de um determinado nó da árvore;
 */
int TREE_GetNodeHeight (tree * t);

/*
TREE_PRINT
 Imprime a árvore;
*/
void TREE_Print (tree * t);

#endif