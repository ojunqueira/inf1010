/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "hash.h"

unsigned int HASH_New (char * input) {
    //    printf("HASH_New\n");
    char * p = input;
    unsigned int hash = 0;
    int i = 0;
    while(*p!='\0'){
        hash = hash + (*p) * (pow(2,i));
        p++;
        i++;
    }
    return hash;
}