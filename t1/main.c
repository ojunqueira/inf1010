/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

# include "main.h"

void MAIN_PrintWorkHeader () {
    printf("\nPUC-RIO (2012.2)");
    printf("\nTRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)");
    printf("\nPROFESSOR: LUCIANO P. SOARES");
    printf("\nALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5");
}

void MAIN_PrintOptions () {
    printf("\n\nDigite uma das opcoes abaixo:");
    printf("\n 1 - Popular Arvore");
    printf("\n 2 - Imprimir Arvore");
    printf("\n 3 - Buscar Estrela");
    printf("\n 4 - Remover Estrela");
    printf("\n 0 - Sair do programa\n");
}

int MAIN_PopulateTree (tree * t) {
    FILE * f = fopen("estrelas.txt", "r");
    if (f == NULL){
        printf("Erro na abertura do arquivo!\n");
    }
    READ_JumpHeader(f);
    char name[50];
    char class[30];
    float velocity = 0;
    unsigned int hash = 0;
    while (READ_NewStar(f, name, class, &velocity)){
        hash = HASH_New(name);
        star * s = STAR_New(hash, name, class, velocity);
        tree * node = t;
        int i = 1;
        int h = TREE_GetTreeHeight(t);
        for (i = 1; i <= h; i++){
            if (READ_Bit(hash, i)) {
                node = node->left;
            }
            else {
                node = node->right;
            }
        }
        if (BUCK_Full(node->b)){
            if (node->h == 32){
                bucket * aux;
                aux = node->b;
                while (aux->next) {
                    aux = aux->next;
                }
                if (BUCK_Full(aux)) {
                    aux->next = BUCK_New();
                }
                BUCK_StarInsert(aux->next, s);
            }
            else {
                while (BUCK_Full(node->b)) {
                    TREE_Expand(t);
                    h = TREE_GetTreeHeight(t);
                    if (READ_Bit(hash, h)) {
                        node = node->left;
                    }
                    else {
                        node = node->right;
                    }
                }
                BUCK_StarInsert(node->b, s);
            }
        }
        else {
            BUCK_StarInsert(node->b, s);
        }
    }
    return 0;
}

int MAIN_StarDelete (tree * t) {
    printf("\nDigite o nome da estrela a ser removida: ");
    char name[50];
    scanf(" %[^\n]", name);
    star * s;
    s = SRCH_Search(t, name);
    if (s) {
        printf("\nEstrela encontrada, deseja mesmo remover?");
        printf("\n 1 - Sim");
        printf("\n 2 - Nao\n");
        int remove = 0;
        scanf(" %d", &remove);
        if (remove == 1) {
            tree * t_aux = t;
            int h = TREE_GetTreeHeight(t);
            int i = 1;
            int hash = HASH_New(name);
            for (i = 1; i <= h; i++) {
                if (READ_Bit(hash, i)) {
                    t_aux = t_aux->left;
                }
                else {
                    t_aux = t_aux->right;
                }
            }
            if (t_aux->b){
                BUCK_StarRemove(t_aux->b, name);
                return 0;
            }
        }
        else {
            printf("\nEstrela nao foi removida");
        }
    }
    else {
        printf("\nEstrela nao cadastrada!");
    }
    return 1;
}

star * MAIN_StarSearch (tree * t) {
    printf("\nDigite o nome da estrela procurada: ");
    char name[50];
    scanf(" %[^\n]", name);
    return SRCH_Search(t, name);
    
}

int main (int argc, const char * argv[]) {
    MAIN_PrintWorkHeader();
    tree * t = TREE_NewTree();
    int option = 1;
    int control = 0;
    while (option) {
        MAIN_PrintOptions();
        scanf(" %d", &option);
        switch (option) {
            case 0:
                printf("\nSaindo do programa ..");
                break;
            case 1:
                if (control > 0) {
                    printf("\nERRO: Arvore ja foi populada!");
                }
                else {
                    printf("\nPopulando arvore ...");
                    if (!MAIN_PopulateTree(t)) {
                        printf("\nArvore populada com sucesso!");
                        control = 1;
                    }
                    else {
                        printf("\nERRO: Nao foi possivel popular a arvore!");
                        control = 0;
                    }
                }
                break;
            case 2:
                if (control < 1) {
                    printf("\nERRO: Carregue uma arvore antes!");
                }
                else {
                    printf("===============================================");
                    TREE_Print(t);
                }
                break;
            case 3:
                if (control < 1) {
                    printf("\nERRO: Carregue uma arvore antes!");
                }
                else {
                    star * s = MAIN_StarSearch(t);
                    if (s) {
                        printf("\nEstrela encontrada:");
                        STAR_Print(s);
                    }
                    else {
                        printf("\nEstrela nao cadastrada!");
                    }
                }
                break;
            case 4:
                if (control < 1) {
                    printf("\nERRO: Carregue uma arvore antes!");
                }
                else {
                    if (!MAIN_StarDelete(t)) {
                        printf("\nEstrela removida com sucesso!");
                    }
                    else {
                        printf("\nEstrela nao foi removida.");
                    }
                    
                }
                break;
            default:
                printf("\nERRO: Digite uma das opções acima!");
                break;
        }
    }
    return 0;
}
