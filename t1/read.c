/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "read.h"

void READ_JumpHeader(FILE * f){
    char title[50];
    char c = 'a';
    fscanf(f,"%[^\n]%c", title, &c);
}

int READ_NewStar (FILE * f, char * name, char * class, float * velocity) {
    if(fscanf(f," %[^,], %[^,], %f", name, class, velocity) == 3){
        return 1;
    }
    return 0;
}

int READ_Bit(unsigned int number, int index){
    unsigned int bit = number;
    bit = bit << ((sizeof(int) * 8) - index);
    bit = bit >> ((sizeof(int) * 8) - 1);
    return bit;
}