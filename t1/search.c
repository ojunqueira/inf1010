/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "search.h"

star * SRCH_SearchBucket(bucket * b, char * name){
    if (b) {
        if (b->s1) {
            if (strcmp(b->s1->name, name) == 0) {
                return b->s1;
            }
        }
        if (b->s2) {
            if (strcmp(b->s2->name, name) == 0) {
                return b->s2;
            }
        }
        if (b->s3) {
            if (strcmp(b->s3->name, name) == 0) {
                return b->s3;
            }
        }
        if (b->next) {
            return SRCH_SearchBucket(b->next, name);
        }
    }
    return NULL;
}

star * SRCH_Search (tree * t, char * name) {
    int h = TREE_GetTreeHeight(t);
    int i = 1;
    tree * t_aux = t;
    int hash = HASH_New(name);
    for (i = 1; i <= h; i++) {
        if (READ_Bit(hash, i)) {
            t_aux = t_aux->left;
        }
        else {
            t_aux = t_aux->right;
        }
    }
    if (t_aux->b){
        star * s = SRCH_SearchBucket(t_aux->b, name);
        return s;
    }
    return NULL;
}