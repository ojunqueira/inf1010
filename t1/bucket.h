/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_Header_h
#define EDA_T1_Header_h

#include <stdio.h>
#include <stdlib.h>
#include "read.h"
#include "star.h"

struct Bucket {
    star * s1;              /* ponteiro para struct da estrela 1 */
    star * s2;              /* ponteiro para struct da estrela 2 */
    star * s3;              /* ponteiro para struct da estrela 3 */
    struct Bucket * next;   /* ponteiro para struct do proximo bucket */
};
typedef struct Bucket bucket;

/*
BUCK_NEW
 Cria novo bucket alocado dinamicamente;
 input  = {}
 output = {
    bucket *:
        - ponteiro para novo bucket;
        - nil: erro na criacao de novo bucket;
 }
*/
bucket * BUCK_New ();

/*
BUCK_CLEAR
 Retira bucket e libera seu espaço de memória;
 input  = {
    bucket * b: Bucket a ser eliminado;
 }
 output = {}
*/
void BUCK_Clear (bucket * b);

/*
BUCK_STARINSERT
 Insere estrela em bucket;
 input  = {
    bucket * b: bucket que receberá a nova estrela;
    star * s:   estrela que deverá ser adicionada ao bucket fornecido;
 }
 output = {
    int:
        - 0: Estrela adicionada com êxito ao bucket;
        - 1: Erro na adição de estrela ao bucket;
 }
*/
int BUCK_StarInsert (bucket * b, star * s);

/*
BUCK_STARREARRANGE
 Retira eventuais ponteiros para estrelas vazios, eliminando 'buracos' entre eles;
 input = {
    bucket * b: Bucket que terá estrelas reorganizadas;
 }
*/
void BUCK_StarRearrange (bucket * b);

/*
BUCK_STARREMOVE
 Remove uma estrela de um bucket;
 input  = {
    bucket * b:  Bucket que sofrerá a remoção de uma estrela;
    char * name: Nome da estrela a ser removida;
 }
 output = {
    int:
        - 0: Remoção da estrela com sucesso;
        - 1: ERRO na remoção da estrela;
 }
*/
int BUCK_StarRemove (bucket * b, char * name);

/*
BUCK_EMPTY
 Verifica se bucket esta vazio;
 input  = {
    bucket * b: Bucket que terá sua ocupação verificada;
 }
 output = {
    int:
        - 0: Bucket nao esta vazio (pode estar cheio ou nao);
        - 1: Bucket vazio (sem nenhuma estrela);
 }
*/
int BUCK_Empty (bucket * b);

/*
BUCK_FULL
 Verifica se bucket esta cheio;
 input  = {
    bucket * b: Bucket que terá sua ocupação verificada;
 }
 output = {
    int:
        - 0: Bucket nao esta cheio (pode estar vazio ou nao);
        - 1: Bucket cheio (possui tres estrelas alocadas)
 }
*/
int BUCK_Full (bucket * b);

/*
BUCK_SPLIT
 Recebe um bucket e devolve um novo apenas com estrelas que possuam novo bit lido;
 input  = {
    bucket * b: Bucket de origem, com ponteiros para estrelas completos;
    int digit:  Dígito de verificação (0 ou 1, que deverá ser comparado ao do hash na posição nbits);
    int nbits:  Posição do valor hash que deverá ser lida para comparar ao digit;
 }
 output = {
    bucket *: 
        - Bucket com os elementos de b cujo digito do valor hash na posição 'nbits' é igual à 'digit'
 }
*/
bucket * BUCK_Split(bucket * b, int digit, int nbits);

/*
BUCK_PRINT
 Imprime o bucket:
 input  = {
    bucket * b: Bucket que será impresso;
 }
 output = {}
*/
void BUCK_Print (bucket * b);

#endif