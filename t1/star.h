/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_star_h
#define EDA_T1_star_h
    
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read.h"

struct Star {
    unsigned int hash;  /* inteiro sem sinal criado pela função hash */
    char name[50];      /* nome da estrela */
    char class[30];     /* classe da estrela*/
    float velocity;     /* velocidade da estrela */
};
typedef struct Star star;

/*
STAR_NEW
 Cria nova estrutura estrela com os parâmetros passados;
 input  = {
    unsigned int hsh: inteiro sem sinal contendo o número hash gerado para o nome da estrela;
    char * nme:       ponteiro para uma string contendo o nome da estrela;
    char * cls:       ponteiro para uma string contendo a classe da estrela;
    float vel:        numero contendo a velocidade da estrela;
 }
 output = {
    star *:
        - null:   erro na criação de nova estrela;
        - star *: ponteiro para estrutura criada com os parâmetros passados;
 }
*/
star * STAR_New(unsigned int hsh, char * nme, char * cls, float vel);

/*
STAR_PRINT
 imprime o conteúdo de uma estrela;
*/
void STAR_Print(star * s);

#endif