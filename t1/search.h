/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_search_h
#define EDA_T1_search_h

#include <stdio.h>
#include "bucket.h"
#include "hash.h"
#include "star.h"
#include "tree.h"

/*
SRCH_SEARCH
 Busca um nome dentro de uma arvore;
*/
star * SRCH_Search (tree * t, char * name);

/*
SRCH_SEARCHBUCKET
 Busca um nome dentro de um bucket;
*/
star * SRCH_SearchBucket(bucket * b, char * name);

#endif
