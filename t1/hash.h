/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T1_hash_h
#define EDA_T1_hash_h

#include <stdio.h>
#include <math.h>

/*
HASH_NEW
 funcao que cria um numero hash a partir de uma string qualquer
 input  = {
    char * n: Ponteiro para uma string de caracteres;
 }
 output = {
    int: 
        - Valor inteiro sem sinal representando o hash do nome recebido;
        - nil: erro na criação do valor hash;
 }
*/
unsigned int HASH_New (char * n);

#endif