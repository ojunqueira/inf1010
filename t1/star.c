/* PUC-RIO (2012.2)                                        */
/* TRABALHO DE ESTRUTURAS DISCRETAS (ENTREGA: 19/09/2012)  */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "star.h"

star * STAR_New(unsigned int hsh, char * nme, char * cls, float vel) {
    star * s = (star *)malloc(sizeof(star));
    s->hash = hsh;
    strcpy(s->name, nme);
    strcpy(s->class, cls);
    s->velocity = vel;
    return s;
}

void STAR_Print(star * s) {
    char bin[35] = "*";
    int i = 32;
    for (i = 32; i > 0; i--){
        int bit = READ_Bit(s->hash, i);
        if (bit == 1) {
            strcat(bin, "1");
        }
        else {
            strcat(bin, "0");
        }
    }
    printf("\n    ESTRELA: < %s (%s), %s, %.2f >", s->name, bin, s->class, s->velocity);
}