INF 1010
=======

PUC-Rio - 2012.2 - INF1010 - Estruturas de Dados Avançadas

Professor: Luciano P. Soares

Aluno: Otávio Junqueira C. Leão (otaviojcl@me.com)


Etapas
=======

Trabalho 1 - Implementação de Hash Expansível

Trabalho 2 - Implementação de Hash Estendível (Extendible Hashing)

Descrição dos trabalhos disponível em 'Downloads'.


Requisitos
=======

GCC, the GNU Compiler Collection (https://gcc.gnu.org/)