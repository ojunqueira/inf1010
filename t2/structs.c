/* PUC-RIO (2012.2)                  (ENTREGA: 14/11/2012) */
/* TRABALHO DE ESTRUTURAS DE DADOS AVANCADAS               */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#include "structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// ESTRUTURAS ENCAPSULADAS NO MODULO

#define N_POSICOES 512

struct {
    char name[50];
    char class[30];
    float velocity;
} typedef star;

struct {
    star * s1;
    star * s2;
    star * s3;
    struct bucket * next;
} typedef bucket;

typedef struct tagHash {
    int n;
    bucket * v[N_POSICOES];
} tpHash;

// FUNÇÕES ENCAPSULADAS NO MODULO

star * NewStar (char * name, char * class, float velocity){
    star * s = (star *)malloc(sizeof(star));
    strcpy(s->name, name);
    strcpy(s->class, class);
    s->velocity = velocity;
    return s;
}

bucket * NewBucket (){
    bucket * b = (bucket *)malloc(sizeof(bucket));
    b->s1 = NULL;
    b->s2 = NULL;
    b->s3 = NULL;
    b->next = NULL;
    return b;
}

int GetHash (tppHash h, char * c) {
    char * p = c;
    int hash = 0;
    int i = 0;
    int cont = 0;
    while(*p!='\0'){
        hash = hash + (*p) * (pow(2,i));
        p++;
        i++;
        cont++;
    }
    hash = hash + (cont * 3);
    return hash % h->n;
}

int GetStarsInBucket (bucket * b){
    if (b) {
        if (b->s3) {
            return 3;
        }
        else if (b->s2) {
            return 2;
        }
        else if (b->s1) {
            return 1;
        }
    }
    return 0;
}

int DuplicateHash (tppHash h) {
    if (h->n == N_POSICOES) {
        return 1; // maximo de posicoes atingidas
    }
//    printf("\nDUPLICANDO PARA %d POSICOES", (h->n * 2));
    tpHash * new = (tpHash *)malloc(sizeof(tpHash));
    int i;
    for (i = 0; i < N_POSICOES; i++) {
        new->v[i] = NULL;
    }
    new->n = h->n * 2;
    for (i = 0; i < h->n; i++) {
        if (h->v[i]) {
            if (h->v[i]->s1) {
                AUX_Insert(new, h->v[i]->s1->name, h->v[i]->s1->class, h->v[i]->s1->velocity);
                //free(h->v[i]->s1);
            }
            if (h->v[i]->s2) {
                AUX_Insert(new, h->v[i]->s2->name, h->v[i]->s2->class, h->v[i]->s2->velocity);
                //free(h->v[i]->s2);
            }
            if (h->v[i]->s3) {
                AUX_Insert(new, h->v[i]->s3->name, h->v[i]->s3->class, h->v[i]->s3->velocity);
                //free(h->v[i]->s3);
            }
        }
    }
    h->n = new->n;
    for (i = 0; i < N_POSICOES; i++) {
        h->v[i] = new->v[i];
    }
    return 0;
}

void InsertColision (tppHash h, char * name, char * class, float velocity) {
    int hash = GetHash(h, name);
    star * s = NewStar(name, class, velocity);
    bucket * b = NewBucket();
    if (h->v[hash]->next) {
        if (GetStarsInBucket(h->v[hash]->next) == 3) {
            b = h->v[hash]->next;
            while (GetStarsInBucket(b) == 3) {
                b = b->next;
            }
        }
        if (b->s1) {
            if (b->s2) {
                b->s3 = s;
            }
            else {
                b->s2 = s;
            }
        }
        else {
            b->s1 = s;
        }
    }
    else {
        b->s1 = s;
        h->v[hash]->next = b;
    }
}

void Rearrange (tppHash h) {
    int i;
    for (i = 0; i < N_POSICOES; i++) {
        if (h->v[i]) {
            if (h->v[i]->s3 && !h->v[i]->s1) {
                h->v[i]->s1 = h->v[i]->s3;
                h->v[i]->s3 = NULL;
            }
            if (h->v[i]->s2 && !h->v[i]->s1) {
                h->v[i]->s1 = h->v[i]->s2;
                h->v[i]->s2 = NULL;
            }
            if (h->v[i]->s3 && !h->v[i]->s2) {
                h->v[i]->s2 = h->v[i]->s3;
                h->v[i]->s3 = NULL;
            }
        }
    }
}

// FUNÇÕES EXPORTADAS PELO MÓDULO

int AUX_Insert (tppHash h, char * name, char * class, float velocity) {
    int hash = GetHash(h, name);
    star * s = NewStar(name, class, velocity);
    switch (GetStarsInBucket(h->v[hash])){
        case 0: {
            bucket * b = NewBucket();
            b->s1 = s;
            h->v[hash] = b;
            return 0;
            break;
        }
        case 1: {
            h->v[hash]->s2 = s;
            return 0;
            break;
        }
        case 2: {
            h->v[hash]->s3 = s;
            return 0;
            break;
        }
        case 3: {
            if (!DuplicateHash(h)) {
                AUX_Insert(h, name, class, velocity);
            }
            else {
                InsertColision(h, name, class, velocity);
            }
            return 0;
            break;
        }
    }
    return 1; //erro
}

tppHash AUX_New (){
    tpHash * h = (tpHash *)malloc(sizeof(tpHash));
    int i;
    h->n = 2;
    for (i = 0; i < N_POSICOES; i++) {
        h->v[i] = NULL;
    }
    return h;
}

void AUX_Print (tppHash h) {
    int i;
    for (i = 0; i < N_POSICOES; i++) {
        if (h->v[i]) {
            printf("\n< v[%d]", i);
            printf("\n  < bucket");
            if (h->v[i]->s1) {
                printf("\n    S1: nome: %s classe: %s velocidade: %f", h->v[i]->s1->name, h->v[i]->s1->class, h->v[i]->s1->velocity);
            }
            if (h->v[i]->s2) {
                printf("\n    S2: nome: %s classe: %s velocidade: %f", h->v[i]->s2->name, h->v[i]->s2->class, h->v[i]->s2->velocity);
            }
            if (h->v[i]->s3) {
                printf("\n    S3: nome: %s classe: %s velocidade: %f", h->v[i]->s3->name, h->v[i]->s3->class, h->v[i]->s3->velocity);
            }
            printf("\n  >");
            printf("\n>");
        }
    }
}

int AUX_Remove (tppHash h) {
    printf("\nDigite o nome da estrela a ser removida: ");
    char name[50];
    scanf(" %[^\n]", name);
    int hash = GetHash(h, name);
    if (GetStarsInBucket(h->v[hash])) {
        if (h->v[hash]->s1 && strcmp(h->v[hash]->s1->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s1->name, h->v[hash]->s1->class, h->v[hash]->s1->velocity);
            printf("\n\nDeseja Remover?\n1 - Sim\n2 - Nao\n");
            int remove = 0;
            scanf(" %d", &remove);
            if (remove == 1) {
                h->v[hash]->s1 = NULL;
                printf("\nEstrela removida com sucesso!");
            }
            else {
                printf("\nEstrela nao foi removida..");
            }
            Rearrange(h);
            return 0;
        }
        else if (h->v[hash]->s2 && strcmp(h->v[hash]->s2->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s2->name, h->v[hash]->s2->class, h->v[hash]->s2->velocity);
            printf("\n\nDeseja Remover?\n1 - Sim\n2 - Nao\n");
            int remove = 0;
            scanf(" %d", &remove);
            if (remove == 1) {
                h->v[hash]->s2 = NULL;
                printf("\nEstrela removida com sucesso!");
            }
            else {
                printf("\nEstrela nao foi removida..");
            }
            Rearrange(h);
            return 0;
        }
        else if (h->v[hash]->s3 && strcmp(h->v[hash]->s3->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s3->name, h->v[hash]->s3->class, h->v[hash]->s3->velocity);
            printf("\n\nDeseja Remover?\n1 - Sim\n2 - Nao\n");
            int remove = 0;
            scanf(" %d", &remove);
            if (remove == 1) {
                h->v[hash]->s3 = NULL;
                printf("\nEstrela removida com sucesso!");
            }
            else {
                printf("\nEstrela nao foi removida..");
            }
            Rearrange(h);
            return 0;
        }
    }
    printf("\nEstrela nao encontrada!");
    return 1;
}

int AUX_Search (tppHash h) {
    printf("\nDigite o nome da estrela procurada: ");
    char name[50];
    scanf(" %[^\n]", name);
    int hash = GetHash(h, name);
    if (GetStarsInBucket(h->v[hash])) {
        if (h->v[hash]->s1 && strcmp(h->v[hash]->s1->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s1->name, h->v[hash]->s1->class, h->v[hash]->s1->velocity);
            return 0;
        }
        else if (h->v[hash]->s2 && strcmp(h->v[hash]->s2->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s2->name, h->v[hash]->s2->class, h->v[hash]->s2->velocity);
            return 0;
        }
        else if (h->v[hash]->s3 && strcmp(h->v[hash]->s3->name, name) == 0) {
            printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", h->v[hash]->s3->name, h->v[hash]->s3->class, h->v[hash]->s3->velocity);
            return 0;
        }/*
        if (h->v[hash]->next != NULL) {
            bucket * b = h->v[hash]->next;
            while (b != NULL) {
                if (strcmp(b->s1->name, name) == 0) {
                    printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", b->s1->name, b->s1->class, b->s1->velocity);
                    return 0;
                }
                else if (strcmp(b->s2->name, name) == 0) {
                    printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", b->s2->name, b->s2->class, b->s2->velocity);
                    return 0;
                }
                else if (strcmp(b->s3->name, name) == 0) {
                    printf("\nEstrela Encontrada!\nnome: %s classe: %s velocidade: %f", b->s3->name, b->s3->class, b->s3->velocity);
                    return 0;
                }
                if (b->next) {
                    b = b->next;
                }
            }
        }*/
    }
    printf("\nEstrela nao encontrada!");
    return 1;
}

void AUX_Stats (tppHash h) {
    int conta_estrelas = 0;
    int conta_buckets = 0;
    int i;
    for (i = 0; i < N_POSICOES; i++) {
        if (h->v[i] != NULL) {
            conta_buckets++;
            if (h->v[i]->s1) {
                conta_estrelas++;
            }
            if (h->v[i]->s2) {
                conta_estrelas++;
            }
            if (h->v[i]->s3) {
                conta_estrelas++;
            }
        }
    }
    printf("\n\nESTATISTICAS DA POPULACAO DO VETOR:");
    printf("\nNumero de Estrelas: %d", conta_estrelas);
    printf("\nNumero de Buckets:  %d", conta_buckets);
    printf("\nTamanho do Vetor:   %d", h->n);
}