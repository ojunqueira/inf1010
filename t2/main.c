/* PUC-RIO (2012.2)                  (ENTREGA: 14/11/2012) */
/* TRABALHO DE ESTRUTURAS DE DADOS AVANCADAS               */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

# include "main.h"
#include <stdio.h>

// SCREEN

void PrintHeader () {
    printf("\n// PUC-RIO (2012.2)         (ENTREGA: 14/11/2012)");
    printf("\n// TRABALHO DE ESTRUTURAS DE DADOS AVANCADAS");
    printf("\n// PROFESSOR: LUCIANO P. SOARES");
    printf("\n// ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5");
}

void PrintOptions () {
    printf("\n\nDigite uma das opcoes abaixo:");
    printf("\n 1 - Popular Estrutura Hash");
    printf("\n 2 - Imprimir Estrutura Hash");
    printf("\n 3 - Imprimir Estatisticas");
    printf("\n 4 - Buscar Estrela");
    printf("\n 5 - Remover Estrela");
    printf("\n 0 - Sair do programa\n");
}

// LEITURA DE ARQUIVO

void ReadHeader(FILE * f){
    char title[50];
    char c = 'a';
    fscanf(f,"%[^\n]%c", title, &c);
}

int ReadStar (FILE * f, char * name, char * class, float * velocity) {
    if(fscanf(f," %[^,], %[^,], %f", name, class, velocity) == 3){
        return 1;
    }
    return 0;
}

int ReadFile (tppHash h) {
    FILE * f = fopen("estrelas.txt", "r");
    if (f == NULL){
        printf("Erro na abertura do arquivo!\n");
        return 1;
    }
    ReadHeader(f);
    char name[50];
    char class[30];
    float velocity = 0;
    while (ReadStar(f, name, class, &velocity)){
        AUX_Insert(h, name, class, velocity);
    }
    return 0;
}

// MAIN

int main (int argc, const char * argv[]) {
    PrintHeader();
    tppHash h = AUX_New();
    int option = 1;
    int control = 0;
    while (option) {
        PrintOptions();
        scanf(" %d", &option);
        switch (option) {
            case 0:
                printf("\nSaindo do programa ..");
                break;
            case 1:
                if (control > 0) {
                    printf("\nERRO: Estrutura hash ja foi populada!");
                }
                else {
                    printf("\nPopulando estrutura hash ...");
                    if (!ReadFile(h)) {
                        printf("\nEstrutura hash populada com sucesso!");
                        control = 1;
                    }
                    else {
                        printf("\nERRO: Nao foi possivel popular a estrutura hash!");
                        control = 0;
                    }
                }
                break;
            case 2:
                if (control < 1) {
                    printf("\nERRO: Carregue um arquivo antes!");
                }
                else {
                    printf("===============================================");
                    AUX_Print(h);
                }
                break;
            case 3:
                if (control < 1) {
                    printf("\nERRO: Carregue um arquivo antes!");
                }
                else {
                    AUX_Stats(h);
                }
                break;
            case 4:
                if (control < 1) {
                    printf("\nERRO: Carregue um arquivo antes!");
                }
                else {
                    AUX_Search(h);
                }
                break;
            case 5:
                if (control < 1) {
                    printf("\nERRO: Carregue um arquivo antes!");
                }
                else {
                    AUX_Remove(h);
                }
                break;
            default:
                printf("\nERRO: Digite uma das opções acima!");
                break;
        }
    }
    return 0;
}
