/* PUC-RIO (2012.2)                  (ENTREGA: 14/11/2012) */
/* TRABALHO DE ESTRUTURAS DE DADOS AVANCADAS               */
/* PROFESSOR: LUCIANO P. SOARES                            */
/* ALUNO:     OTAVIO JUNQUEIRA C. LEAO - 111311-5          */

#ifndef EDA_T2_structs_h
#define EDA_T2_structs_h

typedef struct tagHash * tppHash;


int AUX_Insert (tppHash h, char * name, char * class, float velocity);

tppHash AUX_New ();

void AUX_Print (tppHash h);

int AUX_Remove (tppHash h);

int AUX_Search (tppHash h);

void AUX_Stats (tppHash h);

#endif